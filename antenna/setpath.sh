#!/bin/bash

ACTION=$(basename "${0%.sh}")

USAGE=$(cat <<-END
 --path=0|1	0: On-board, 1: External
END
)

. $(dirname "$0")/../common.sh

# Parse args
for i in "$@"
do
case $i in
	--path=*)
	ANT_PATH=${i#*=}
	shift
	;;
esac
done

ENDPOINT=$(dirname "${0#./}")
DATA='{ "action": "'$ACTION'", "path": '$ANT_PATH' }'

http_request $ENDPOINT "$DATA"

#!/bin/bash

ACTION=$(basename "${0%.sh}")

USAGE=$(cat <<-END
 --discoverable=[0|1]			    Switch an adapter to discoverable or non-discoverable.
 --pairable=[0|1]			        Switch an adapter to pairable or non-pairable.
 --discoverableTimeout=INT		The discoverable timeout in seconds.
 --pairableTimeout=INT		    The pairable timeout in seconds.
END
)

. $(dirname "$0")/../common.sh

# Parse args
for i in "$@"
do
case $i in
	--discoverable=*)
	DISCOVERABLE=${i#*=}
	shift
	;;
	--pairable=*)
	PAIRABLE=${i#*=}
	shift
	;;
	--discoverableTimeout=*)
	DISCOVERABLE_TIMEOUT=${i#*=}
	shift
	;;
	--pairableTimeout=*)
	PAIRABLE_TIMEOUT=${i#*=}
	shift
	;;
esac
done

if [ ! -z "$DISCOVERABLE" ]; then
  [ "$DISCOVERABLE" = "1" ] && DISCOVERABLE="true" || DISCOVERABLE="false"
fi

if [ ! -z "$PAIRABLE" ]; then
  [ "$PAIRABLE" = "1" ] && PAIRABLE="true" || PAIRABLE="false"
fi

ENDPOINT=$(dirname "${0#./}")
DATA='{ "action": "'$ACTION'"'
if [ ! -z "$DISCOVERABLE" ]; then
	DATA="$DATA"', "discoverable": '$DISCOVERABLE''
fi
if [ ! -z "$PAIRABLE" ]; then
  DATA="$DATA"', "pairable": '$PAIRABLE''
fi
if [ ! -z "$DISCOVERABLE_TIMEOUT" ]; then
  DATA="$DATA"', "discoverableTimeout": '$DISCOVERABLE_TIMEOUT''
fi
if [ ! -z "$PAIRABLE_TIMEOUT" ]; then
  DATA="$DATA"', "pairableTimeout": '$PAIRABLE_TIMEOUT''
fi
DATA="$DATA"' }'

http_request "$ENDPOINT" "$DATA"

#!/bin/bash

ACTION=$(basename "${0%.sh}")

. $(dirname "$0")/../common.sh

ENDPOINT=$(dirname "${0#./}")
DATA='{ "action": "'$ACTION'" }'

http_request "$ENDPOINT" "$DATA"

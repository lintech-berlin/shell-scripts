#!/bin/bash

ACTION=$(basename "${0%.sh}")

USAGE=$(cat <<-END
 --address=ADDRESS		Bluetooth MAC address
END
)

. $(dirname "$0")/../common.sh

# Parse args
for i in "$@"
do
case $i in
	--address=*)
	ADDRESS=${i#*=}
	shift
	;;
esac
done

ENDPOINT=$(dirname "${0#./}")
DATA='{ "action": "'$ACTION'", "address": "'"$ADDRESS"'" }'

http_request "$ENDPOINT" "$DATA"

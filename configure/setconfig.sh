#!/bin/bash

ACTION="setconfig"

USAGE=$(cat <<-END
 --ssid="SSID"		SSID, e.g. --ssid="My Home Network"
 --psk="PSK"		pre-shared key, only for WPA and WPA2
 --name="NAME"		Device name, e.g. --name="Kitchen"
END
)

. $(dirname "$0")/../common.sh

# Parse args
for i in "$@"
do
case $i in
	--name=*)
	DEVICENAME=${i#*=}
	shift
	;;
	--ssid=*)
	SSID=${i#*=}
	shift
	;;
	--psk=*)
	PSK=${i#*=}
	shift
	;;
esac
done

ENDPOINT=configure
DATA='{ "action": "'$ACTION'", "deviceinfo":{ "devicename": "'"$DEVICENAME"'" }, "networkinfo":{ "wifimode": "client", "clientinfo":{ "encrypt_type": "WPA", "wifipwd": "'"$PSK"'", "wifissid": "'"$SSID"'" }, "ipaddressinfo":{ "type": "DHCP"}}}'

http_request $ENDPOINT "$DATA"

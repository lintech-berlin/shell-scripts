#!/bin/bash

ACTION=$(basename "${0%.sh}")

USAGE=$(cat <<-END
 --username=USERNAME		Qobuz username
 --password=PASSWORD		Qobuz password
 --audioquality=AUDIOQUALITY	Qobuz audio quality (1=LOW, 2=HIGH, 3=LOSSLESS, 4=HI_RES)
END
)

. $(dirname "$0")/../common.sh

# Parse args
for i in "$@"
do
case $i in
	--username=*)
	QOBUZ_USERNAME=${i#*=}
	shift
	;;
	--password=*)
	QOBUZ_PASSWORD=${i#*=}
	shift
	;;
	--audioquality=*)
	QOBUZ_AUDIOQUALITY=${i#*=}
	shift
	;;
	*)
	# unknown option
	;;
esac
done

QOBUZ_USERNAME=${QOBUZ_USERNAME:-"jk-lintech"}
QOBUZ_PASSWORD=${QOBUZ_PASSWORD:-"berlin01"}
QOBUZ_AUDIOQUALITY=${QOBUZ_AUDIOQUALITY:-5}

ENDPOINT=$(dirname "${0#./}")
DATA='{ "action": "'$ACTION'", "username": "'$QOBUZ_USERNAME'", "password": "'$QOBUZ_PASSWORD'", "audioquality": '$QOBUZ_AUDIOQUALITY' }'

http_request "$ENDPOINT" "$DATA"

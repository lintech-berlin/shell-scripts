#!/bin/bash

ACTION=$(basename "${0%.sh}")

USAGE=$(cat <<-END
 Saves the Tidal track list stored in savetracklist.json
END
)

. $(dirname "$0")/../common.sh

ENDPOINT=$(dirname "${0#./}")
JSON_FILE=$(dirname "$0")/"$ACTION".json

http_request_file "$ENDPOINT" "$JSON_FILE"

#!/bin/bash

ACTION=$(basename "${0%.sh}")

USAGE=$(cat <<-END
 --channel=0|1|2|3	0: Both, 1: Left, 2: Right, 3: Mono
END
)

. $(dirname "$0")/../common.sh

# Parse args
for i in "$@"
do
case $i in
	--channel=*)
	CHANNEL=${i#*=}
	shift
	;;
esac
done

ENDPOINT=$(dirname "${0#./}")
DATA='{ "action": "'$ACTION'", "channel": '$CHANNEL' }'

http_request $ENDPOINT "$DATA"

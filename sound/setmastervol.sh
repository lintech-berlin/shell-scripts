#!/bin/bash

ACTION="setmastervol"

USAGE=$(cat <<-END
 --vol=VOL	Volume level, e.g. --vol=120
END
)

. $(dirname "$0")/../common.sh

# Parse args
for i in "$@"
do
case $i in
	--vol=*)
	VOL=${i#*=}
	shift
	;;
esac
done

ENDPOINT=sound
DATA='{ "action": "'$ACTION'", "volume": '$VOL' }'

http_request $ENDPOINT "$DATA"

#!/bin/bash

ACTION=$(basename "${0%.sh}")

USAGE=$(cat <<-END
 --name="NAME"
 --settings="XX,XX,XX,XX,XX,XX,XX,XX,XX,XX"
END
)

. $(dirname "$0")/../common.sh

# Parse args
for i in "$@"
do
case $i in
	--name=*)
	NAME=${i#*=}
	shift
	;;
	--settings=*)
	SETTINGS=${i#*=}
	shift
	;;
esac
done

case "$NAME" in
'Default')	SETTINGS="66,66,66,66,66,66,66,66,66,66" ;;
'Classical')	SETTINGS="71,71,71,71,71,71,84,83,83,87" ;;
'Club')		SETTINGS="71,71,67,63,63,63,67,71,71,71" ;;
'Dance')	SETTINGS="57,61,69,71,71,81,83,83,71,71" ;;
'Headphones')	SETTINGS="65,55,64,77,75,70,65,57,52,49" ;;
'Bass')		SETTINGS="59,59,59,63,70,78,85,88,89,89" ;;
'Treble')	SETTINGS="87,87,87,78,68,55,47,47,47,45" ;;
'Large hall')	SETTINGS="56,56,63,63,71,79,79,79,71,71" ;;
'Live')		SETTINGS="79,71,66,64,63,63,66,68,68,69" ;;
'Party')	SETTINGS="61,61,71,71,71,71,71,71,61,61" ;;
'Pop')		SETTINGS="74,65,61,60,64,73,75,75,74,74" ;;
'Reggae')	SETTINGS="71,71,72,81,71,62,62,71,71,71" ;;
'Rock')		SETTINGS="58,63,80,84,77,66,58,55,55,55" ;;
'Ska')		SETTINGS="75,79,78,72,66,63,58,57,55,57" ;;
'Soft Rock')	SETTINGS="66,66,69,72,78,80,77,72,68,58" ;;
'Soft')		SETTINGS="65,70,73,75,73,66,59,57,55,53" ;;
'Techno')	SETTINGS="60,63,71,80,79,71,60,57,57,58" ;;
'Jazz')		SETTINGS="53,42,54,58,63,68,74,79,89,79" ;;
esac

ENDPOINT=$(dirname "${0#./}")
DATA='{ "action": "'$ACTION'", "settings": ['$SETTINGS']'
if [ ! -z "$NAME" ]; then
	DATA="$DATA"', "name": "'"$NAME"'"'
fi
DATA="$DATA"' }'

http_request $ENDPOINT "$DATA"

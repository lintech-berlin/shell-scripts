#!/bin/bash

ACTION="setdevicename"

USAGE=$(cat <<-END
 --name="NAME"		Device name, e.g. --name="Kitchen"
END
)

. $(dirname "$0")/../common.sh

# Parse args
for i in "$@"
do
case $i in
	--name=*)
	NAME=${i#*=}
	shift
	;;
esac
done

ENDPOINT=device
DATA='{ "action": "'$ACTION'", "devicename": "'"$NAME"'" }'

http_request $ENDPOINT "$DATA"

#!/bin/bash

ACTION="reboot"

. $(dirname "$0")/../common.sh

ENDPOINT=device
DATA='{ "action": "'$ACTION'" }'

http_request $ENDPOINT "$DATA"

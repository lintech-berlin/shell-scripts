#!/bin/bash

ACTION="playplaylist"

USAGE=$(cat <<-END
 --listid=ID	ID of the playlist, e.g. --listid=1.
 		\e[3mOptional, defaults to 1.\e[0m
 --pos=POS	Start playback from POS in the playlist, e.g. --pos=3.
 		\e[3mOptional, defaults to 1.\e[0m
END
)

. $(dirname "$0")/../common.sh

# Parse args
for i in "$@"
do
case $i in
	--listid=*)
	LISTID=${i#*=}
	shift
	;;
	--pos=*)
	POS=${i#*=}
	shift
	;;
	*)
	# unknown option
	;;
esac
done

LISTID=${LISTID:-"1"}
POS=${POS:-"1"}

ENDPOINT=radio
DATA='{ "action": "'$ACTION'", "listid": '$LISTID', "pos": '$POS'}'

http_request $ENDPOINT "$DATA"

#!/bin/bash

ACTION="play"

USAGE=$(cat <<-END
 Plays the station stored in play.json
END
)

. $(dirname "$0")/../common.sh

ENDPOINT=radio
JSON_FILE=$(dirname "$0")/play.json

http_request_file $ENDPOINT $JSON_FILE

#!/bin/bash

ACTION="getplaylist"

USAGE=$(cat <<-END
 --listid=ID	ID of the playlist, e.g. --listid=1.
END
)

. $(dirname "$0")/../common.sh

# Parse args
for i in "$@"
do
case $i in
	--listid=*)
	LISTID=${i#*=}
	shift
	;;
esac
done

LISTID=${LISTID:-"1"}

ENDPOINT=radio
DATA='{ "action": "'$ACTION'", "listid": '$LISTID' }'

http_request $ENDPOINT "$DATA"

#!/bin/bash

ACTION="saveplaylist"

USAGE=$(cat <<-END
 Saves the playlist stored in saveplaylist.json
END
)

. $(dirname "$0")/../common.sh

ENDPOINT=radio
JSON_FILE=$(dirname "$0")/saveplaylist.json

http_request_file $ENDPOINT $JSON_FILE

#!/bin/bash

ACTION="getfavouritestation"

. $(dirname "$0")/../common.sh

ENDPOINT=radio
DATA='{ "action": "'$ACTION'" }'

http_request $ENDPOINT "$DATA"

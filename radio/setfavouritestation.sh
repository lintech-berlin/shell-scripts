#!/bin/bash

ACTION="setfavouritestation"

USAGE=$(cat <<-END
 Set the favourite stations stored in setfavouritestation.json
END
)

. $(dirname "$0")/../common.sh

ENDPOINT=radio
JSON_FILE=$(dirname "$0")/setfavouritestation.json

http_request_file $ENDPOINT $JSON_FILE

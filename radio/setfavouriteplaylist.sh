#!/bin/bash

ACTION="setfavouriteplaylist"

USAGE=$(cat <<-END
 		\e[31mUse without any options to remove the current favourite playlist.\e[0m
 --listid=ID	ID of the playlist, e.g. --listid=1.
END
)

. $(dirname "$0")/../common.sh

# Parse args
for i in "$@"
do
case $i in
	--listid=*)
	LISTID=${i#*=}
	shift
	;;
	*)
	# unknown option
	;;
esac
done

ENDPOINT=radio
DATA='{ "action": "'$ACTION'"'
if [ ! -z "$LISTID" ]; then
	DATA="$DATA"', "listid": "'"$LISTID"'"'
fi
DATA="$DATA"' }'

http_request $ENDPOINT "$DATA"

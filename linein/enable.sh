#!/bin/bash

ACTION=$(basename "${0%.sh}")

USAGE=$(cat <<-END
 --type=[0,1]	0: analog, 1: digital
END
)

. $(dirname "$0")/../common.sh

# Parse args
for i in "$@"
do
case $i in
	--type=*)
	TYPE=${i#*=}
	shift
	;;
esac
done

ENDPOINT=$(dirname "${0#./}")
DATA='{ "action": "'$ACTION'", "type": '$TYPE' }'

http_request $ENDPOINT "$DATA"

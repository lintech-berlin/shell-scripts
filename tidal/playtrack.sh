#!/bin/bash

ACTION=$(basename "${0%.sh}")

USAGE=$(cat <<-END
 --trackid="TRACKID"   Tidal trackid, e.g. "68691383"
END
)

. $(dirname "$0")/../common.sh

# Parse args
for i in "$@"
do
case $i in
	--trackid=*)
	TRACKID=${i#*=}
	shift
	;;
esac
done

ENDPOINT=$(dirname "${0#./}")
DATA='{ "action": "'$ACTION'", "trackid": "'"$TRACKID"'" }'

http_request $ENDPOINT "$DATA"

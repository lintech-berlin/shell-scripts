#!/bin/bash

ACTION=$(basename "${0%.sh}")

USAGE=$(cat <<-END
 --username=USERNAME		Tidal username
 --password=PASSWORD		Tidal password
 --audioquality=AUDIOQUALITY	Tidal audio quality (1=LOW, 2=HIGH, 3=LOSSLESS, 4=HI_RES)
END
)

. $(dirname "$0")/../common.sh

# Parse args
for i in "$@"
do
case $i in
	--username=*)
	TIDAL_USERNAME=${i#*=}
	shift
	;;
	--password=*)
	TIDAL_PASSWORD=${i#*=}
	shift
	;;
	--audioquality=*)
	TIDAL_AUDIOQUALITY=${i#*=}
	shift
	;;
	*)
	# unknown option
	;;
esac
done

TIDAL_USERNAME=${TIDAL_USERNAME:-"lintechhifi"}
TIDAL_PASSWORD=${TIDAL_PASSWORD:-"tidal2018"}
TIDAL_AUDIOQUALITY=${TIDAL_AUDIOQUALITY:-2}

ENDPOINT=$(dirname "${0#./}")
DATA='{ "action": "'$ACTION'", "username": "'$TIDAL_USERNAME'", "password": "'$TIDAL_PASSWORD'", "audioquality": '$TIDAL_AUDIOQUALITY' }'

http_request "$ENDPOINT" "$DATA"

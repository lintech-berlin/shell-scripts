#!/bin/bash

ACTION="removenetwork"

USAGE=$(cat <<-END
 --id=ID	Network ID from listnetworks, e.g. --id=1
END
)

. $(dirname "$0")/../common.sh

# Parse args
for i in "$@"
do
case $i in
	--id=*)
	ID=${i#*=}
	shift
	;;
esac
done

ENDPOINT=wifi
DATA='{ "action": "'$ACTION'", "id": '$ID' }'

http_request $ENDPOINT "$DATA"

#!/bin/bash

ACTION=$(basename "${0%.sh}")

USAGE=$(cat <<-END
 Sets the DNS settings from setdns.json
END
)

. $(dirname "$0")/../common.sh

ENDPOINT=$(dirname "${0#./}")
JSON_FILE=$(dirname "$0")/setdns.json

http_request_file $ENDPOINT $JSON_FILE

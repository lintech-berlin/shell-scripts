#!/bin/bash

ACTION="setapmode"

USAGE=$(cat <<-END
 --ssid="SSID"		SSID, e.g. --ssid="My Home Network"
 --channel=CHANNEL	Channel, e.g. --channel=6
 --encrypt=0|1		1 enables encryption, 0 disables encryption
 --psk="PSK"		pre-shared key, needed if --encrypt=1
END
)

. $(dirname "$0")/../common.sh

# Parse args
for i in "$@"
do
case $i in
	--ssid=*)
	SSID=${i#*=}
	shift
	;;
	--psk=*)
	PSK=${i#*=}
	shift
	;;
	--encrypt=*)
	ENCRYPT=${i#*=}
	shift
	;;
	--channel=*)
	CHANNEL=${i#*=}
	shift
	;;
esac
done

ENDPOINT=network

DATA='{ "action": "'$ACTION'"'
if [ ! -z "$SSID" ]; then
	DATA="$DATA"', "ssid": "'"$SSID"'"'
fi
if [ ! -z "$PSK" ]; then
	DATA="$DATA"', "psk": "'"$PSK"'"'
fi
if [ ! -z "$ENCRYPT" ]; then
	DATA="$DATA"', "encrypt": '"$ENCRYPT"''
fi
if [ ! -z "$CHANNEL" ]; then
	DATA="$DATA"', "channel": '"$CHANNEL"''
fi
DATA="$DATA"' }'

http_request $ENDPOINT "$DATA"

#!/bin/bash

ACTION="addstaticinet"

USAGE=$(cat <<-END
 --idstr="ID_STR"	ID_STR, e.g. --idstr="home"
 --address="ADDRESS"	IP address, e.g. "192.168.178.123"
 --gateway="GATEWAY"	Gateway, e.g. "192.168.178.0"
 --netmask="NETMASK"	Netmask, defaults to "255.255.255.0"
END
)

. $(dirname "$0")/../common.sh

# Parse args
for i in "$@"
do
case $i in
	--idstr=*)
	ID_STR=${i#*=}
	shift
	;;
	--address=*)
	ADDRESS=${i#*=}
	shift
	;;
	--gateway=*)
	GATEWAY=${i#*=}
	shift
	;;
	--netmask=*)
	NETMASK=${i#*=}
	shift
	;;
esac
done

NETMASK=${NETMASK:-"255.255.255.0"}

ENDPOINT=network

DATA='{ "action": "'$ACTION'", "idstr": "'"$ID_STR"'", "inet": { "address": "'"$ADDRESS"'", "netmask": "'"$NETMASK"'", "gateway": "'"$GATEWAY"'" } }'

http_request $ENDPOINT "$DATA"

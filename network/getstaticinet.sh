#!/bin/bash

ACTION="getstaticinet"

USAGE=$(cat <<-END
 --idstr="ID_STR"	ID_STR, e.g. --idstr="home"
END
)

. $(dirname "$0")/../common.sh

# Parse args
for i in "$@"
do
case $i in
	--idstr=*)
	ID_STR=${i#*=}
	shift
	;;
esac
done

ENDPOINT=network
DATA='{ "action": "'$ACTION'", "idstr": "'"$ID_STR"'" }'

http_request $ENDPOINT "$DATA"

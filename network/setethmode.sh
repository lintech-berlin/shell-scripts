#!/bin/bash

ACTION=$(basename "${0%.sh}")

USAGE=$(cat <<-END
 --method=0|1			0: DHCP, 1: static
 --address="ADDRESS"		IP address, e.g. "192.168.178.123"
 --gateway="GATEWAY"		Gateway, e.g. "192.168.178.0"
 --broadcast="BROADCAST"	Broadcast, e.g. "192.168.178.255"
 --netmask="NETMASK"		Netmask, defaults to "255.255.255.0"
 --dns="DNS1,DNS2,DNS2"		Comma seperated list of domain name servers
 --domains="DOMAIN1,DOMAIN2"	Comma seperated list of domain name servers
END
)

. $(dirname "$0")/../common.sh

# Parse args
for i in "$@"
do
case $i in
	--method=*)
	METHOD=${i#*=}
	shift
	;;
	--address=*)
	ADDRESS=${i#*=}
	shift
	;;
	--gateway=*)
	GATEWAY=${i#*=}
	shift
	;;
	--broadcast=*)
	BROADCAST=${i#*=}
	shift
	;;
	--netmask=*)
	NETMASK=${i#*=}
	shift
	;;
	--dns=*)
	DNS=${i#*=}
	shift
	;;
	--domains=*)
	DOMAINS=${i#*=}
	shift
	;;
esac
done

ENDPOINT=$(dirname "${0#./}")

DATA='{ "action": "'$ACTION'", "method": '$METHOD''
if [ ! -z "$ADDRESS" ]; then
	DATA="$DATA"', "inet": { "address": "'"$ADDRESS"'"'
	if [ ! -z "$NETMASK" ]; then
		DATA="$DATA"', "netmask": "'"$NETMASK"'"'
	fi
	if [ ! -z "$GATEWAY" ]; then
		DATA="$DATA"', "gateway": "'"$GATEWAY"'"'
	fi
	if [ ! -z "$BROADCAST" ]; then
		DATA="$DATA"', "broadcast": "'"$BROADCAST"'"'
	fi
	DATA="$DATA"' }'
fi
if [ ! -z "$DNS" ]; then
	DATA="$DATA"', "dns": { "nameservers": [ '

	# parse the comma seperated list of name servers into an JSON array
	IFS=',' read -ra ARR <<< "$DNS"
	DNS=""
	for i in "${ARR[@]::${#ARR[@]}}"; do
		DNS="$DNS""\"$i\""
	done
	DNS=$(echo $DNS | sed 's/""/", "/g')
	DATA="$DATA""$DNS"

	DATA="$DATA"' ]'

	if [ ! -z "$DOMAINS" ]; then
		DATA="$DATA"', "domains": [ '

		# parse the comma seperated list of domains into an JSON array
		IFS=',' read -ra ARR <<< "$DOMAINS"
		DOMAINS=""
		for i in "${ARR[@]::${#ARR[@]}}"; do
			DOMAINS="$DOMAINS""\"$i\""
		done
		DOMAINS=$(echo $DOMAINS | sed 's/""/", "/g')
		DATA="$DATA""$DOMAINS"

		DATA="$DATA"' ]'
	fi

	DATA="$DATA"' }'
fi
DATA="$DATA"' }'

echo $DATA

http_request $ENDPOINT "$DATA"

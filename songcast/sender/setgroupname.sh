#!/bin/bash

ACTION=$(basename "${0%.sh}")

USAGE=$(cat <<-END
 --groupname="GROUPNAME"	Group name, e.g. --groupname="Home"
END
)

. $(dirname "$0")/../../common.sh

# Parse args
for i in "$@"
do
case $i in
	--groupname=*)
	GROUPNAME=${i#*=}
	shift
	;;
esac
done

ENDPOINT=$(dirname "${0#./}")
DATA='{ "action": "'$ACTION'", "groupname": "'"$GROUPNAME"'" }'

http_request $ENDPOINT "$DATA"

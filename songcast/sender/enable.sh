#!/bin/bash

ACTION=$(basename "${0%.sh}")

USAGE=$(cat <<-END
 --groupname="GROUPNAME"	Group name
 --mode=0|1			Unicast or Multicast
END
)

. $(dirname "$0")/../../common.sh

# Parse args
for i in "$@"
do
case $i in
	--groupname=*)
	GROUPNAME=${i#*=}
	shift
	;;
	--mode=*)
	MODE=${i#*=}
	shift
	;;
esac
done

MODE=${MODE:-"0"}

ENDPOINT=$(dirname "${0#./}")
DATA='{ "action": "'$ACTION'", "groupname": "'"$GROUPNAME"'", "mode": '"$MODE"' }'

http_request $ENDPOINT "$DATA"


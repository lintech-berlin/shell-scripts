#!/bin/bash

ACTION=$(basename "${0%.sh}")

USAGE=$(cat <<-END
 --uuid="UUID"   Senders UUID, e.g. "57d01a5a-d7e8-4451-c462-0023b1e3c315"
END
)

. $(dirname "$0")/../../common.sh

# Parse args
for i in "$@"
do
case $i in
	--uuid=*)
	UUID=${i#*=}
	shift
	;;
esac
done

ENDPOINT=$(dirname "${0#./}")
DATA='{ "action": "'$ACTION'", "uuid": "'"$UUID"'" }'

http_request $ENDPOINT "$DATA"

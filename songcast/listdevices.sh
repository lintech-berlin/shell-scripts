#!/bin/bash

ACTION="listdevices"

. $(dirname "$0")/../common.sh

ENDPOINT=$(dirname "$0")
DATA='{ "action": "'$ACTION'" }'

http_request $ENDPOINT "$DATA"

#!/bin/bash

set -e

function print_usage() {
	echo -n "Usage: ./$ACTION.sh"
	if [ ! -z "$USAGE" ]; then
		echo " [options...]"
		echo "Options:"
	fi
	echo -e "$USAGE"
	exit 0
}

function print_full_usage() {
	echo "Usage: ./$ACTION.sh [options...]"
	echo "Options:"
	echo " -s		Secured"
	echo " --ip=<IP>	IP"
	echo " --api=<API>	API"
	echo -e "$USAGE"
	exit 0
}

# Parse args
for i in "$@"
do
case $i in
	-h)
	print_usage
	;;
	--help)
	print_full_usage
	;;
	-s)
	PROTOCOL=https
	PORT=4949
	CURLOPTS="-k --tlsv1.2"
	;;
	--ip=*)
	IP=${i#*=}
	;;
	--api=*)
	API=${i#*=}
	;;
	*)
	# unknown option
	;;
esac
done

API=${API:-"v20"}
IP=${IP:-"192.168.2.1"}
PORT=${PORT:-"8989"}
PROTOCOL=${PROTOCOL:-"http"}
CURLOPTS=$CURLOPTS" -s -S"
QUIET="no"

type jq >/dev/null 2>&1 && PRETTY_PRINT=1
PRETTY_PRINT=${PRETTY_PRINT:-0}

function http_request() {
	local ENDPOINT=$1
	local DATA=$2

	if [ "$QUIET" = "no" ]; then
		printf "Protocol: $PROTOCOL, API: $API, IP: $IP, Port: $PORT ...\n"
		echo -e "\e[2mPOST:\e[22m"
		echo "$DATA" | ( [ $PRETTY_PRINT = 1 ] && jq . || ( cat && echo ) )
	fi
	echo -e "\e[2mREPLY:\e[22m"
	response="$(curl $CURLOPTS \
    -X POST -H "Content-Type: application/json; charset=UTF-8" \
    -d "$DATA" $PROTOCOL://$IP:$PORT/api/$API/$ENDPOINT.action)"
  if jq -e . >/dev/null 2>&1 <<<"$response"; then
    [ $PRETTY_PRINT = 1 ] && jq <<< "${response}" || ( cat && echo )
  else
	  [ $PRETTY_PRINT = 1 ] && jq -R -M <<< "${response}" || ( cat && echo )
  fi
}

function http_request_file() {
	local ENDPOINT=$1
	local FILE=$2

	if [ "$QUIET" = "no" ]; then
		printf "Protocol: $PROTOCOL, API: $API, IP: $IP, Port: $PORT ...\n"
		echo -e "\e[2mPOST:\e[22m"
		cat "$FILE" | ( [ $PRETTY_PRINT = 1 ] && jq . || ( cat && echo ) )
	fi
	echo -e "\e[2mREPLY:\e[22m"
	curl $CURLOPTS \
		-X POST -H "Content-Type: application/json; charset=UTF-8" \
		-d @$FILE \
		$PROTOCOL://$IP:$PORT/api/$API/$ENDPOINT.action \
	| ( [ $PRETTY_PRINT = 1 ] && jq . || ( cat && echo ) )
}

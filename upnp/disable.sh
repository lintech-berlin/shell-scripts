#!/bin/bash

ACTION="disable"

. $(dirname "$0")/../common.sh

ENDPOINT=upnp
DATA='{ "action": "'$ACTION'" }'

http_request $ENDPOINT "$DATA"

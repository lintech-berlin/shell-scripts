#!/bin/bash

ACTION="seturl"

USAGE=$(cat <<-END
 --version="VERSION"	Version to use for the update, e.g. --version="4.3.0"
 --server="SERVER"	LinTech server to use for the update, e.g. --server="lt"
 --model="MODEL"	Board model, e.g. --model="HBM11"
END
)

. $(dirname "$0")/../common.sh

# Parse args
for i in "$@"
do
case $i in
	--version=*)
	VERSION=${i#*=}
	shift
	;;
	--server=*)
	SERVER=${i#*=}
	shift
	;;
	--model=*)
	MODEL=${i#*=}
	shift
	;;
esac
done

SERVER=${SERVER:-"lt-dl"}

if [ -z $VERSION ]; then
	echo "Missing version! Use --version=<version>."
	exit 1
fi
if [ -z $MODEL ]; then
	echo "Missing model! Use --model=<model>."
	exit 1
fi

URL="http://$SERVER.lintech.de/download/upgrade/$MODEL/$VERSION/"

ENDPOINT=otaupgrade
DATA='{ "action": "'$ACTION'", "otaaddress": "'"$URL"'" }'

http_request $ENDPOINT "$DATA"

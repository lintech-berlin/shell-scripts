#!/bin/bash

ACTION=$(basename "${0%.sh}")

USAGE=$(cat <<-END
 --brightness=VALUE	Brightness value, e.g. --brightness=120
END
)

. $(dirname "$0")/../common.sh

# Parse args
for i in "$@"
do
case $i in
	--brightness=*)
	VALUE=${i#*=}
	shift
	;;
esac
done

ENDPOINT=$(dirname "${0#./}")
DATA='{ "action": "'$ACTION'", "brightness": '$VALUE' }'

http_request $ENDPOINT "$DATA"

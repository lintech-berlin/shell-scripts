# HBM10 - HTTP API shell scripts

The HTTP API shell scripts are a set of shell scripts to run in a Linux terminal.

The purpose of the scripts is to demonstrate the functionality of the HBM10 and
to help developers to evaluate the API and therefore ease the integration into
custom applications.

## Requirements

An optional dependency is the tool [jq](https://stedolan.github.io/jq/) for
pretty printing the JSON data.

## Basic usage

The basic usage is:

	$ ./command.sh [-s] [--api=<API>] [--ip=<IP>] [options]

The command line parameters in brackets are optional and are set to a default
value if omitted:

| Parameter     | Description     | Default value |
| ------------- |-----------------|---------------|
| -s            | Use HTTPS       | Use HTTP      |
| --api         | Set API version | v16           |
| --ip          | Set network IP  | 192.168.2.1   |

To ease command line usage the API and IP values can be exported to the
environment with new values, e.g.:

	$ export IP=192.168.178.144
	$ export API=v16

This way the command can be executed without the need to add the `--api` and
`--ip` parameters for every run.

*Note, that these environment parameters are only valid for the current terminal
session.*

## Options

Each command prints a short usage by passing `-h` to the command, e.g.:

```
$ ./network/setapmode.sh -h
Usage: ./setapmode.sh [options...]
Options:
 --ssid="SSID"		SSID, e.g. --ssid="My Home Network"
 --channel=CHANNEL	Channel, e.g. --channel=6
 --encrypt=0|1		1 enables encryption, 0 disables encryption
 --psk="PSK"		pre-shared key, needed if --encrypt=1
```

## Examples

### Get the network information

```
$ ./network/info.sh
Protocol: http, API: v16, IP: 192.168.178.44, Port: 8989 ...
POST:
{
  "action": "info"
}
REPLY:
{
  "wlan": {
    "channel": 6,
    "ssid": "Lintech1",
    "inet": [
      {
        "netmask": "255.255.255.0",
        "ip": "192.168.178.44",
        "family": "IPv4"
      }
    ],
    "mac": "00:23:b1:74:10:c6",
    "mode": "station",
    "encryption": "WPA2-PSK"
  }
}

```

### Enable encryption for AP mode operation:

```
./network/setapmode.sh --encrypt=1 --psk="mysecretkey"
Protocol: http, API: v16, IP: 192.168.178.44, Port: 8989 ...
POST:
{
  "action": "setapmode",
  "psk": "mysecretkey",
  "encrypt": 1
}
REPLY:
{
  "returncode": "success"
}
```

### Integration into a home network

Perform a network scan:

```
$ ./wifi/scan.sh 
Protocol: http, API: v16, IP: 192.168.178.44, Port: 8989 ...
POST:
{
  "action": "scan"
}
REPLY:
[
  {
    "channel": 6,
    "ssid": "Lintech1",
    "bssid": "34:81:c4:29:51:96",
    "rsn": {
      "auth": "PSK",
      "pairwise_ciphers": "CCMP",
      "group_cipher": "CCMP"
    },
    "signal": -43
  },
  {
    "channel": 11,
    "ssid": "Lintech2",
    "bssid": "34:81:c4:c5:09:64",
    "rsn": {
      "auth": "PSK",
      "pairwise_ciphers": "CCMP",
      "group_cipher": "CCMP"
    },
    "signal": -73
  },
  {
    "channel": 11,
    "ssid": "Lintech3",
    "bssid": "00:1c:28:b4:40:5c",
    "wpa": {
      "auth": "PSK",
      "pairwise_ciphers": "TKIP CCMP",
      "group_cipher": "TKIP"
    },
    "rsn": {
      "auth": "PSK",
      "pairwise_ciphers": "TKIP CCMP",
      "group_cipher": "TKIP"
    },
    "signal": -78
  }
]
```

Add network:

```
$ ./wifi/addnetwork.sh --ssid="Lintech1" --psk="mysecretkey"
Protocol: http, API: v16, IP: 192.168.178.44, Port: 8989 ...
POST:
{
  "action": "addnetwork",
  "ssid": "Lintech1",
  "psk": "mysecretkey"
}
REPLY:
{
  "id": 3,
  "returncode": "success"
}
```

Select the network to connect to:

```
./wifi/selectnetwork.sh --id=3
Protocol: http, API: v16, IP: 192.168.178.44, Port: 8989 ...
POST:
{
  "action": "selectnetwork",
  "id": 3
}
REPLY:
{
  "returncode": "success"
}
```